import bpy
import os

print("Exporting GLTF")

# Define the function to adjust light intensity
def adjust_light_intensity(scale_factor=0.1):
    for obj in bpy.context.scene.objects:
        if obj.type == 'LIGHT':
            light = obj.data
            light.energy *= scale_factor
            print(f"Adjusted {obj.name} light intensity to {light.energy}")

# Explicitly set the active scene to "webgl lid"
active_scene = bpy.data.scenes.get("webgl lid")
if not active_scene:
    raise ValueError("Scene 'webgl lid' not found!")

bpy.context.window.scene = active_scene  # Set the context to the desired scene
print(f"Active scene set to: {active_scene.name}")

# Select all objects in the active scene
bpy.ops.object.select_all(action='DESELECT')  # Deselect all objects
for obj in active_scene.objects:
    obj.select_set(True)  # Select only the objects in the active scene

# Get the directory of the current .blend file
blend_file_dir = os.path.dirname(bpy.data.filepath)

# Define the relative path (going up one directory and into 'img')
relative_path = "../img/3dlid.gltf"
output_path = os.path.abspath(os.path.join(blend_file_dir, relative_path))

# Optional: Ensure the directory exists
os.makedirs(os.path.dirname(output_path), exist_ok=True)

# Adjust light intensity (optional)
adjust_light_intensity(scale_factor=0.1)  # Adjust this factor as needed

# Export only the selected objects (those from the active scene)
bpy.ops.export_scene.gltf(
    filepath=output_path,
    export_format='GLTF_SEPARATE',      # Export as .gltf (JSON with embedded data)
    export_apply=True,                  # Apply object transformations
    export_materials='EXPORT',          # Export only materials for the selected objects
    export_cameras=True,                # Include cameras in the export
    export_lights=True,                 # Include lights in the export
    export_animations=True,             # Include animations
    use_selection=True,                 # Only export selected objects (from the active scene)
    export_yup=True,                    # Use +Y as the up axis (GLTF default)
    export_skins=True,                  # Export skinning data (for animated characters)
    export_morph=True,                  # Export shape keys (morph targets)
    export_frame_range=True,            # Export the frame range of animations
    export_frame_step=1,                # Step between animation frames
    export_image_format='AUTO',         # Export image format (AUTO uses the original image format)
    export_draco_mesh_compression_enable=False  # Disable Draco compression (set True to enable)
)

print(f"Scene 'webgl lid' exported to {output_path}")

