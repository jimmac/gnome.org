![GNOME Website](img/card.png)

# Gnome.org Website Maintenance

Welcome to the Gnome.org website maintenance guide. This document contains instructions on how to maintain the static HTML website of Gnome.org.

## About the Gnome.org Website

The Gnome.org website is a static HTML website that does not rely on any frameworks. The website can be maintained by editing HTML and CSS files. 

## Release Process

The release process involves making sure the screenshots are up to date with the latest release, which is available at [release.gnome.org](https://release.gnome.org). 

### Screenshots

The best way to get good screenshots is to run [GNOMEOS](https://os.gnome.org) in [Boxes](https://flathub.org/apps/org.gnome.Boxes) and set the resolution to `1920x1200` (Laptop model has a `16:10` screen). Scale ratio is 100%.

You need to update three screenshots:
* `src/tex/overview.png` -- light variant of the main hero
* `src/tex/overview-dark.png` -- dark variant of the hero
* `src/tex/download.png` -- *Get GNOME* animation

If your distro comes with [Blender](https://blender.org) that has been compiled with [numpy](https://numpy.org/), you can also render and export running the scripts in `src`:

```
cd src/
blender -b webgl.blend -P export-webgl.py
blender -b gfx.blend -P render-hero.py
```

Sadly neither Ubuntu, nor Fedora does. The flatpak version of Blender does, but the sandbox makes it tricky to access the scripts/project file. The best approach is to launch blender, open the gfx.blend project, locate the text window and press `Alt+P` with your cursor in the script window.

The output of the process should be visible in the `./img` directory.

### Sponsor Logos

The sponsor logos are SVG. Before including the SVG in the page, shorten and sanitize it with [`svgo`](https://github.com/svg/svgo). 

## Local Testing

To ensure the javascript runs and links work correctly (site uses smart URLs), a local server is needed. We've provided a simple web server in python. To launch it in the project folder:

```
python3 ./test.py
```

## Deployment

The GitLab CI will deal with site deployment. Once you have pushed the changes to the main branch, GitLab CI will automatically build and deploy the website to the live server.

## Contributing

If you have any questions or concerns or want to contribute to the Gnome.org website, please get in touch with us on [Websites Team channel](https://matrix.to/#/#pages:gnome.org) of the GNOME Matrix server.

Thank you for helping to maintain the Gnome.org website!
